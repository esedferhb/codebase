require "neon_email/version"
require "neon_email/handler"

module NeonEmail
  class Error < StandardError; end
end
