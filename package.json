{
    "private": true,
    "workspaces": [
        "i18n",
        "mdx",
        "server",
        "schemas",
        "web"
    ],
    "optionalDependencies": {
        "cypress": "^6.9.1"
    },
    "devDependencies": {
        "@graphql-codegen/cli": "^1.21.4",
        "@graphql-codegen/typescript-operations": "^1.18.0",
        "@graphql-codegen/typescript-react-apollo": "^2.2.4",
        "@testing-library/cypress": "^7.0.6",
        "@testing-library/jest-dom": "^5.12.0",
        "@testing-library/react": "^11.2.7",
        "@turf/bezier-spline": "^6.3.0",
        "@turf/helpers": "^6.3.0",
        "@types/cors": "^2.8.10",
        "@types/express": "^4.17.11",
        "@types/express-jwt": "^6.0.1",
        "@types/express-serve-static-core": "^4.17.20",
        "@types/faker": "^5.5.5",
        "@types/jest": "^26.0.23",
        "@types/jsonwebtoken": "^8.5.1",
        "@types/node": "^15.3.1",
        "@types/yup": "^0.29.11",
        "@typescript-eslint/eslint-plugin": "^4.22.0",
        "axe-core": "^4.2.1",
        "babel-jest": "^27.0.2",
        "babel-plugin-add-react-displayname": "^0.0.5",
        "cypress-axe": "^0.12.2",
        "cypress-plugin-tab": "^1.0.5",
        "eslint": "^7.25.0",
        "eslint-config-prettier": "^8.3.0",
        "eslint-plugin-import": "^2.22.1",
        "eslint-plugin-jest": "^24.3.6",
        "eslint-plugin-jsdoc": "^33.0.0",
        "eslint-plugin-jsx-a11y": "^6.4.1",
        "eslint-plugin-prettier": "^3.4.0",
        "eslint-plugin-react": "^7.23.2",
        "eslint-plugin-react-hooks": "^4.2.0",
        "eslint-plugin-unicorn": "^32.0.1",
        "faker": "^5.5.3",
        "identity-obj-proxy": "^3.0.0",
        "jest": "^26.6.3",
        "leaflet": "^1.7.1",
        "leaflet-polylinedecorator": "^1.6.0",
        "react-leaflet": "^3.2.0",
        "remark-cli": "^9.0.0",
        "remark-comment-config": "^6.0.0",
        "remark-github": "^10.0.1",
        "remark-lint": "^8.0.0",
        "remark-lint-write-good": "^1.2.0",
        "remark-preset-lint-consistent": "^4.0.0",
        "remark-preset-lint-markdown-style-guide": "^4.0.0",
        "remark-preset-lint-recommended": "^5.0.0",
        "remark-toc": "^7.2.0",
        "remark-validate-links": "^10.0.4",
        "start-server-and-test": "^1.12.2",
        "ts-jest": "^26.5.6"
    },
    "scripts": {
        "check-types": "tsc -p .",
        "cypress:seed-users": "psql -c \"INSERT INTO person (email, role) VALUES ('portal@sink.sendgrid.com', 'portal'), ('admin@sink.sendgrid.com', 'admin');\" $DATABASE_URL",
        "db:seed": "psql -f ./server/src/utils/seeds.sql $DATABASE_URL",
        "develop:seed-neo4j": "cypher-shell -f ./server/src/utils/seeds.cypher $NEO4J_URL",
        "develop:shell": "docker exec -it shell /bin/bash",
        "docker-remove-containers": "docker container kill $(docker ps -q)",
        "docker-prune": "docker system prune -a -f --volumes",
        "e2e": "cypress run --headless --project ./web",
        "lint": "eslint './**/*.{js,jsx,ts,tsx}'",
        "markdown-lint": "remark -i -e mdx,md --frail .remarkignore .",
        "migrate:commit": "yarn workspace @neonlaw/server graphile-migrate commit",
        "migrate:watch": "yarn workspace @neonlaw/server graphile-migrate watch",
        "translations:create": "yarn workspace @neonlaw/i18n translations:create",
        "run-script": "ts-node --transpile-only",
        "copy-gcp-credentials": "doppler secrets get --plain PRODUCTION > neon-law-production.credentials.json; doppler secrets get --plain STAGING > neon-law-staging.credentials.json",
        "gke-production": "gcloud container clusters get-credentials neon-law-production --region us-west4 --project neon-law-production",
        "gke-staging": "gcloud container clusters get-credentials neon-law-staging --region us-west4 --project neon-law-staging",
        "sql-proxy-staging": "docker run -v $(pwd)/neon-law-staging.credentials.json:/config -p 127.0.0.1:5433:5432 gcr.io/cloudsql-docker/gce-proxy:1.17 /cloud_sql_proxy -instances=neon-law-staging:us-west4:neon-law=tcp:0.0.0.0:5432 -credential_file=/config",
        "sql-proxy-production": "docker run -v $(pwd)/neon-law-production.credentials.json:/config -p 127.0.0.1:5434:5432 gcr.io/cloudsql-docker/gce-proxy:1.17 /cloud_sql_proxy -instances=neon-law-production:us-west4:neon-law=tcp:0.0.0.0:5432 -credential_file=/config",
        "neo4j-poll": "while true ; do nc -vz 127.0.0.1 7687 ; sleep 10 ; done",
        "neo4j-proxy-production": "kubectl port-forward $(kubectl get pod --selector=\"app.kubernetes.io/component=core,app.kubernetes.io/instance=production-neo4j,app.kubernetes.io/name=neo4j\" --output jsonpath='{.items[0].metadata.name}') 7687:7687",
        "neo4j-proxy-staging": "kubectl port-forward $(kubectl get pod --selector=\"app.kubernetes.io/component=core,app.kubernetes.io/instance=staging-neo4j,app.kubernetes.io/name=neo4j\" --output jsonpath='{.items[0].metadata.name}') 7687:7687",
        "superset-poll": "while true ; do nc -vz 127.0.0.1 8088 ; sleep 10 ; done",
        "superset-proxy-production": "kubectl port-forward service/production-superset 8088:8088",
        "superset-proxy-staging": "kubectl port-forward service/staging-superset 8088:8088",
        "kafka-cc-poll": "while true ; do nc -vz 127.0.0.1 9021 ; sleep 10 ; done",
        "kafka-cc-proxy-production": "kubectl port-forward service/production-kafka-cp-control-center 9021:9021",
        "kafka-cc-proxy-staging": "kubectl port-forward service/staging-kafka-cp-control-center 9021:9021",
        "test": "jest --forceExit --detectOpenHandles --coverage --coverageProvider=v8"
    },
    "dependencies": {
        "@auth0/nextjs-auth0": "^1.3.0",
        "@chakra-ui/icons": "^1.0.13",
        "@chakra-ui/react": "^1.6.2",
        "@stripe/stripe-js": "^1.15.0",
        "@typescript-eslint/parser": "^4.25.0",
        "@uppy/core": "^1.18.1",
        "@uppy/react": "^1.11.8",
        "@uppy/transloadit": "^1.6.23",
        "@uppy/tus": "^1.8.7",
        "clipboard-copy": "^4.0.1",
        "jwt-decode": "^3.1.2",
        "next-compose-plugins": "^2.2.1",
        "react-intl": "^5.13.5",
        "react-player": "^2.9.0",
        "react-responsive-carousel": "^3.2.18",
        "slate": "^0.63.0",
        "slate-history": "^0.62.0",
        "slate-hyperscript": "^0.62.0",
        "slate-react": "^0.63.0",
        "typescript": "^4.2.4",
        "uuid": "^8.3.2",
        "voca": "^1.4.0"
    },
    "resolutions": {
        "graphql": "^14.0.1"
    }
}
