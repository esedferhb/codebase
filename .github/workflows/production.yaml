---
name: production

on:
  pull_request:
    branches:
      - main
  workflow_run:
    workflows:
      - system_tests
    branches:
      - main
    types:
      - completed

  workflow_dispatch:

jobs:
  deploy_production_server:
    runs-on: ubuntu-latest

    steps:
      - uses: actions/checkout@v2
      - name: Login to GitHub Container Registry
        uses: docker/login-action@v1
        with:
          registry: docker.pkg.github.com
          username: ${{ secrets.GPR_USERNAME }}
          password: ${{ secrets.GPR_PERSONAL_ACCESS_TOKEN }}

      - name: Build the Docker image
        run: |-
          docker build \
            --tag "us.gcr.io/neon-law-production/server:$GITHUB_SHA" \
            --tag "us.gcr.io/neon-law-production/server:latest" \
            -f ./server/Dockerfile \
            .

      - uses: google-github-actions/setup-gcloud@master
        with:
          project_id: neon-law-production
          service_account_email: github-actions@neon-law-production.iam.gserviceaccount.com
          service_account_key: ${{ secrets.PRODUCTION_GCP_CREDENTIALS }}
          export_default_credentials: true
        if: github.ref == 'refs/heads/main'

      - name: |-
          Configure Docker to use the gcloud command-line tool as a credential
          helper for authentication
        run: |-
          gcloud --quiet auth configure-docker
        if: github.ref == 'refs/heads/main'

      - name: Push the Docker image to Google Container Registry
        run: |-
          docker push "us.gcr.io/neon-law-production/server:$GITHUB_SHA"
          docker push "us.gcr.io/neon-law-production/server:latest"
        if: github.ref == 'refs/heads/main'

      - name: Get the GKE credentials so we can deploy to the cluster
        run: |-
          gcloud container clusters get-credentials neon-law-production --region us-west4
        if: github.ref == 'refs/heads/main'

      - name: Update the base image
        run: |-
          kubectl set image deployment production-api \
             api=us.gcr.io/neon-law-production/server:$GITHUB_SHA

          kubectl set image deployment production-workers \
             workers=us.gcr.io/neon-law-production/server:$GITHUB_SHA
        if: github.ref == 'refs/heads/main'

  deploy_production_webhooks:
    runs-on: ubuntu-latest

    steps:
      - uses: actions/checkout@v2
      - name: Login to GitHub Container Registry
        uses: docker/login-action@v1
        with:
          registry: docker.pkg.github.com
          username: ${{ secrets.GPR_USERNAME }}
          password: ${{ secrets.GPR_PERSONAL_ACCESS_TOKEN }}

      - name: Build the Docker image
        run: |-
          docker build \
            --tag "us.gcr.io/neon-law-production/webhooks:$GITHUB_SHA" \
            --tag "us.gcr.io/neon-law-production/webhooks:latest" \
            -f ./webhooks/Dockerfile \
            .

      - uses: google-github-actions/setup-gcloud@master
        with:
          project_id: neon-law-production
          service_account_email: github-actions@neon-law-production.iam.gserviceaccount.com
          service_account_key: ${{ secrets.PRODUCTION_GCP_CREDENTIALS }}
          export_default_credentials: true
        if: github.ref == 'refs/heads/main'

      - name: |-
          Configure Docker to use the gcloud command-line tool as a credential
          helper for authentication
        run: |-
          gcloud --quiet auth configure-docker
        if: github.ref == 'refs/heads/main'

      - name: Push the Docker image to Google Container Registry
        run: |-
          docker push "us.gcr.io/neon-law-production/webhooks:$GITHUB_SHA"
          docker push "us.gcr.io/neon-law-production/webhooks:latest"
        if: github.ref == 'refs/heads/main'

      - name: Get the GKE credentials so we can deploy to the cluster
        run: |-
          gcloud container clusters get-credentials neon-law-production --region us-west4
        if: github.ref == 'refs/heads/main'

      - name: Update the base image
        run: |-
          kubectl set image deployment production-webhooks \
             webhooks=us.gcr.io/neon-law-production/webhooks:$GITHUB_SHA
        if: github.ref == 'refs/heads/main'

  deploy_production_web:
    runs-on: ubuntu-latest

    steps:
      - uses: actions/checkout@v2
      - name: Login to GitHub Container Registry
        uses: docker/login-action@v1
        with:
          registry: docker.pkg.github.com
          username: ${{ secrets.GPR_USERNAME }}
          password: ${{ secrets.GPR_PERSONAL_ACCESS_TOKEN }}

      - name: Build the Docker image
        run: |-
          docker build \
            --tag "us.gcr.io/neon-law-production/web:$GITHUB_SHA" \
            --tag "us.gcr.io/neon-law-production/web:latest" \
            -f ./web/Dockerfile \
            .

      - uses: google-github-actions/setup-gcloud@master
        if: github.ref == 'refs/heads/main'
        with:
          project_id: neon-law-production
          service_account_email: github-actions@neon-law-production.iam.gserviceaccount.com
          service_account_key: ${{ secrets.STAGING_GCP_CREDENTIALS }}
          export_default_credentials: true

      - name: |-
          Configure Docker to use the gcloud command-line tool as a credential
          helper for authentication
        run: |-
          gcloud --quiet auth configure-docker
        if: github.ref == 'refs/heads/main'

      - name: Push the Docker image to Google Container Registry
        run: |-
          docker push "us.gcr.io/neon-law-production/web:$GITHUB_SHA"
          docker push "us.gcr.io/neon-law-production/web:latest"
        if: github.ref == 'refs/heads/main'

      - name: Get the GKE credentials so we can deploy to the cluster
        run: |-
          gcloud container clusters get-credentials neon-law-production --region us-west4
        if: github.ref == 'refs/heads/main'

      - name: Deploy latest container with kubectl
        run: |-
          kubectl set image deployment production-web \
            subscriber=us.gcr.io/neon-law-production/web:$GITHUB_SHA
        if: github.ref == 'refs/heads/main'
